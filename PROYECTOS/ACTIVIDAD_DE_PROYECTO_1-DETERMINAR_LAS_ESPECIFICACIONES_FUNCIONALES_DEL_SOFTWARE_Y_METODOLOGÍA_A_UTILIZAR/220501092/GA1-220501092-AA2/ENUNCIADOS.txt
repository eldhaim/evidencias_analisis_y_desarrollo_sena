3.1.2 Actividad de aprendizaje GA1-220501092-AA2: identificar conceptos básicos de teoría general de
sistemas y enfoque sistémico
Esta actividad se centra en la identificación de fuentes de requisitos.
Duración: 12 horas.
Materiales de formación: para el desarrollo de esta actividad es importante la lectura y análisis del material de
formación: “Ingeniería de requisitos”.
Evidencias:
● Evidencia de Conocimiento: GA1-220501092-AA2-EV01 mapa mental sobre ingeniería de requisitos
A continuación, se describen las acciones y las correspondientes evidencias que conforman la actividad de
aprendizaje:
● Realizar un mapa mental con los conceptos sobre ingeniería de requisitos vistos en el componente formativo
“Ingeniería de requisitos”.
● Utilizar una herramienta TIC para la realización del mapa mental.
● El mapa debe tener las principales características de los mapas mentales.
Lineamientos para la entrega del producto:
● Producto para entregar: mapa mental con los conceptos sobre ingeniería de requisitos.
● Formato: PDF o Word.
● Extensión: libre.
● Para hacer el envío del producto remítase al área de la actividad correspondiente y acceda al espacio para el
envío de la evidencia: mapa mental sobre ingeniería de requisitos-GA1-220501092-AA2-EV01.
● Evidencia de desempeño GA1-220501092-AA2-EV02: foro temático. Fuentes de requisitos
Conteste la pregunta planteada con referencia al tema de identificación de fuentes de requisitos y de la opinión de
las respuestas de, al menos, tres (3) de sus compañeros.
● Responda la pregunta: ¿cuáles son los tipos de fuentes y de un ejemplo de cada una? ¿En qué caso uso cada
tipo de fuente?
● Se debe responder de forma concisa donde el aprendiz de la opinión con referencia al tema.
● El aprendiz debe dar su opinión a la respuesta de, al menos, tres (3) compañeros.
● Se deben cumplir con normas ortográficas en las respuestas realizadas.

GFPI-F-135 V01
Lineamientos para la entrega del producto:
● Producto para entregar: respuesta foro temático.
● Formato: PDF o Word.
● Extensión: libre.
● Para hacer el envío del producto remítase al área de la actividad correspondiente y acceda al espacio para el
envío de la evidencia: foro temático. Fuentes de requisitos GA1-220501092-AA2-EV02 