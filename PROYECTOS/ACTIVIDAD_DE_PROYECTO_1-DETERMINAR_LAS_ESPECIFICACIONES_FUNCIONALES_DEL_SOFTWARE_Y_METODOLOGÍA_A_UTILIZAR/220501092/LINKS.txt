/*PRINCIPAL*/
Guia_de_aprendizaje_01:
https://sena.territorio.la/content/index.php/institucion/Titulada/institution/SENA/Tecnologia/228118/Contenido/DocArtic/GUI1/Guia_aprendizaje_01.pdf

/*GA1-220501092-AA1*/
Caracterización_de_procesos:
https://sena.territorio.la/content/index.php/institucion/Titulada/institution/SENA/Tecnologia/228118/Contenido/OVA/CF1/index.html#/

/*GA1-220501092-AA2*/
Ingeniería_de_requisitos
https://sena.territorio.la/content/index.php/institucion/Titulada/institution/SENA/Tecnologia/228118/Contenido/OVA/CF2/index.html#/

/*GA1-220501092-AA3*/
La_fase_de_elicitación_de_requisitos
https://sena.territorio.la/content/index.php/institucion/Titulada/institution/SENA/Tecnologia/228118/Contenido/OVA/CF3/index.html#/

/*GA1-220501092-AA4*/
Análisis_y_especificación_de_requisitos
https://sena.territorio.la/content/index.php/institucion/Titulada/institution/SENA/Tecnologia/228118/Contenido/OVA/CF4/index.html#/

/*GA1-220501092-AA5*/
Validación_de_requisitos
https://sena.territorio.la/content/index.php/institucion/Titulada/institution/SENA/Tecnologia/228118/Contenido/OVA/CF5/index.html#/