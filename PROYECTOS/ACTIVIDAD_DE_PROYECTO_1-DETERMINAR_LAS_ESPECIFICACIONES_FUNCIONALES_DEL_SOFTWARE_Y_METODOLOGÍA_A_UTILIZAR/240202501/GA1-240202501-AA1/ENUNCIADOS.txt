3.4.1 Actividad de aprendizaje: GA1-240202501-AA1 identificar situaciones cotidianas y futuras a través de
una interacción social oral y escrita
El aprendizaje de una segunda lengua se facilita cuando se comienza a estudiar desde temas conocidos como son
las situaciones de la cotidianidad ya sea abordándolas desde el momento actual.
Es importante seguir las indicaciones del instructor en cuanto a la mejor manera de apropiar el material del curso y
la correlación que tiene este con el programa de formación.
Duración: 48 horas.
Material de formación: los contenidos de soporte para el desarrollo de la actividad son “LEVEL 1 - MCER A1.1”.
Evidencias:
● Evidencia de conocimiento: GA1-240202501-AA1-EV01 Cuestionario
Presenta un cuestionario (evaluación en línea) para evaluar comprensión lectora y gramatical del nivel, el cual
consta de quince preguntas (15) y un tiempo aproximado de 45 minutos.
Lineamientos para la entrega de la evidencia:
● Para responder el cuestionario (evaluación en línea), remítase al área de la actividad correspondiente y acceda
al espacio para el envío de la evidencia Cuestionario GA1-240202501AA1-EV01.
● Evidencia de desempeño: GA1-240202501-AA1-EV02 Video
De acuerdo con la temática estudiada en el primer nivel escoja un miembro de su familia o un amigo para
presentarlo por medio de un corto video activando su cámara web en inglés con el fin de identificar de forma oral
las características personales sobre edad, nacionalidad, lugar de residencia, hobbies y alguna información
relevante sobre las actividades cotidianas que realiza.

GFPI-F-135 V01
Para la elaboración del video con la cámara web, tenga en cuenta el desarrollo de un guion o estructura a través de
una herramienta como PowerPoint, Emaze, Prezi en la que se dispongan algunas diapositivas con imágenes y textos
para dar cuenta de la presentación del personaje.
Diapositivas: durante el desarrollo de la presentación oral es ideal que presente entre 3 y 5 diapositivas, donde se
incluyan los siguientes elementos:
● Diapositiva de portada (datos básicos del aprendiz, nombre del curso, instructor y nombre de la actividad).
● Diapositivas de datos del personaje seleccionado (nombre completo, edad, fecha de cumpleaños, ocupación,
actividades de tiempo libre), acompañado de imágenes, frases, textos cortos y elementos gráficos que le
permitan hablar durante la presentación.
Estructura del video con cámara web: cuando grabe el video, tenga en cuenta que las fotos, imágenes y textos que
use en las diapositivas deben ser un apoyo visual para denotar buena pronunciación y su aprendizaje inicial sobre
las temáticas del primer nivel.
Para la realización de la emisión del video deberá encender su cámara web, mostrar la pantalla con las diapositivas
creadas. La recomendación es utilizar alguna herramienta digital que permite grabar el video y pantalla como
Screencast-o-Matic, Loom, Camtasia, recordscreen.io, scrnrcrd.com e incluso existen aplicaciones como X Recorder
para que pueda realizarlo desde su teléfono móvil. Lo importante es mostrar las diapositivas, su cámara web e ir
realizando su presentación de forma oral.
Una vez finalizado el video debe cargarlo a YouTube o Vimeo, con su cuenta de correo personal o institucional;
compruebe que no tenga restricciones de visualización para que pueda compartir el enlace de visualización.
Lineamientos para la entrega de la evidencia:
● Producto a entregar: documento con los datos del aprendiz y enlace del Video.
● Formato: Word o PDF con la URL del video.
● Extensión: de 2 a 5 minutos.
● Para hacer el envío del documento remítase al área de la actividad correspondiente y acceda al espacio
para el envío de la evidencia Video GA1-240202501-AA1-EV02.
● Evidencia de producto: GA1-240202501-AA1-EV03. Folleto
A partir de los datos, conceptos, e investigación obtenida de su programa de formación deberá realizar un folleto en
inglés con la información y características de su programa de formación.
Para realizar el folleto puede utilizar alguna herramienta digital como Canva, Crello, Word, PowerPoint o cualquier
otra que conozca y le permita trabajar de forma creativa.

Estructura: datos personales del aprendiz, mensaje inicial, información básica del programa, expectativas e intereses
que se vinculen a su interés particular por estudiar el programa de formación.
Describa de manera sencilla y clara utilizando frases, vocabulario e imágenes relacionadas con temas de interés y
características de su programa de formación.
Lineamientos para la entrega de la evidencia:

GFPI-F-135 V01
● Producto a entregar: folleto. Haga uso de las normas gramaticales y redacción utilizando el idioma inglés,
además, aplique las normas APA para las referencias y citación de información obtenida de diferentes
recursos digitales.
● Extensión: folleto tríptico entre 70 y 150 palabras.
● Formato: el folleto deberá exportarlo a PDF.
● Para hacer el envío del documento remítase al área de la actividad correspondiente y acceda al espacio para
el envío de la evidencia: Folleto GA1-240202501-AA1-EV03.