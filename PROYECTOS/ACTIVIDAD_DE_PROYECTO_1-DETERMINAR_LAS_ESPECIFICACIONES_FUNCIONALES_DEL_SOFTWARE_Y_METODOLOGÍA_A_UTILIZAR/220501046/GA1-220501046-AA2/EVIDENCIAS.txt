3.2.2 Actividad de aprendizaje: GA1-220501046-AA2 desarrollar un taller práctico sobre los términos y
funcionalidades de la ofimática
El objetivo de esta actividad es el dominio de herramientas ofimáticas y colaborativas, aplicando la estrategia de
aprender haciendo mediante la resolución de un taller propuesto.
Duración: 24 horas.
Materiales de formación: Para el desarrollo de esta actividad es importante la lectura y análisis del material de
formación: “Tecnologías de la Información y la Comunicación (TIC)”.
Evidencias:
A continuación, se describen las acciones y las correspondientes evidencias que conforman la actividad de
aprendizaje:
● Evidencia Conocimiento: GA1-220501046-AA2-EV01 taller- Utilización de las herramientas de
Ofimática. Realizar un taller práctico con las herramientas ofimáticas
Realizar un taller práctico con las herramientas ofimáticas, tomando como base los temas presentados en el
componente formativo “Tecnologías de la Información y la Comunicación (TIC)”. Para la elaboración del taller
se debe tener en cuenta:
- Parte 1. Realizar el taller sobre la aplicación de herramientas informáticas que se presenta en el archivo
comprimido anexo “Taller_Ofimatica-220501046-AA2-EV01”. Este taller se debe realizar de manera
Individual.
- Parte 2. Tomando como base el siguiente enlace sobre el uso de Trello: https://blog.trello.com/es/comousartrello, se debe elaborar un tablero de Trello y realizar las diferentes acciones descritas en el enlace (incluir
múltiples tarjetas, elementos checklist, fondos personalizados y responsabilidades), se deben organizar

GFPI-F-135 V01
equipos de máximo cinco (5) integrantes para compartir el tablero creado el cual debe compartirse además
con el instructor.
Lineamientos para la entrega del producto:
● Producto para entregar: se debe entregar un archivo comprimido que contenga:
● El archivo en formato Word con la parte 1 de la actividad, es decir, el taller sobre herramientas ofimáticas.
● Un documento que describa las acciones realizadas en la parte 2 de la actividad, es decir, la construcción del
tablero en Trello, además de la dirección de acceso al mismo. El informe debe incluir las dificultades
encontradas en el uso de esta herramienta.
● Formato: archivo comprimido en formato RAR o ZIP.
● Para hacer el envío del producto remítase al área de la actividad correspondiente y acceda al espacio para el
envío de la evidencia: taller- Utilización de las herramientas de Ofimática. Realizar un taller práctico con
las herramientas ofimáticas- GA1-220501046-AA2-EV01.